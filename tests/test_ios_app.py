# import os
import pytest
from time import sleep

from appium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.remote.remote_connection import RemoteConnection

# from helpers import DESIRED_CAPS_IOS_APP
from tests.helpers import DESIRED_CAPS_IOS_APP_V2

class TestIosApp():
    
    @pytest.fixture(scope="function")
    def driver(self, request, KOBITON_SERVER_URL):
        driver = webdriver.Remote(command_executor = KOBITON_SERVER_URL, desired_capabilities = DESIRED_CAPS_IOS_APP_V2)
        def fin():
            driver.quit()
            
        request.addfinalizer(fin)
        return driver  # provide the fixture value

    def test_click(self, driver: webdriver.webdriver.WebDriver):
        el = driver.find_element_by_xpath('//*[@name="Welcome to Greet"]')
        assert el.text == "Welcome to Greet"
        print("Elemento: ", el.text)
        sleep(1000)

    # def test_click_skip(self, driver: webdriver.webdriver.WebDriver):
    #     skip_button = driver.find_element_by_xpath('(//XCUIElementTypeOther[@name="SKIP"])[2]')
    #     print("ELEMENTOOOO: ", skip_button.text)
    #     assert skip_button.text == "SKIP"
    #     skip_button.click()