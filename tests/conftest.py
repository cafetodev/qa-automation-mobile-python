import pytest
import os

@pytest.fixture
def KOBITON_SERVER_URL():
    return 'https://{}:{}@api.kobiton.com/wd/hub'.format(
        os.getenv('KOBITON_USERNAME'), os.getenv('KOBITON_ACCESS_KEY')
        )


        
