Feature: Login
    Scenario: Login with valid credentials
        Given I have an account created
        When  I send the following valid credentials
            | username | password                 |
            | eurbano  | Jc3ntukv3PB8%3PBik1wc0fW |
        Then  I expect the login to be successfully

    Scenario: login with no valid credentials
        Given I have an account created
        When  I send the following invalid credentials
            | username | password                 |
            | ""       | ""                       |
            | ""       | "123"                    |
            | Dani     | ""                       |
            | dani?]   | Jc3ntukv3PB8%3PBik1wc0fW |
        Then  I expect the login to be unsuccessful

    Scenario: login when the user does not exists
        Given I have not an account created
        When  I send the following credentials
            | username | password                 |
            | alaina   | Jc3ntukv3PB8%3PBik1wc0fW |
        Then  I expect the login to be unsuccessful