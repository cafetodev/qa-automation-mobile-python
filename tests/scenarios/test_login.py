import pytest
from time import sleep

from appium import webdriver
from tests.helpers import DESIRED_CAPS_IOS_APP_V2
from pytest_bdd import scenario, given, when, then, parsers


@pytest.fixture(scope="function")
def driver(self, request, KOBITON_SERVER_URL):
    driver = webdriver.Remote(command_executor = KOBITON_SERVER_URL, desired_capabilities = DESIRED_CAPS_IOS_APP_V2)
    def fin():
        driver.quit()
        
    request.addfinalizer(fin)
    return driver  # provide the fixture value

@scenario("login.feature", "Login with valid credentials")
def test_valid_login():
    pass

@given("I have an account created")
def click(driver: webdriver.webdriver.WebDriver):
    pass

@when("I login with valid credentials")
def login():
    pass
    # el = driver.find_element_by_xpath('//*[@name="Welcome to Greet"]')
    # assert el.text == "Welcome to Greet"
    # print("Elemento: ", el.text)
    # sleep(1000)
    
    
# SKIP BUTTON
# (//XCUIElementTypeOther[@name="SKIP"])[2]

# ADD ART BUTTON 
# //XCUIElementTypeOther[@name=""]