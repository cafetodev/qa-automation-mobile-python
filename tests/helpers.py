DESIRED_CAPS_ANDROID_WEB = {
  'sessionName':        '[Python] Android web',
  'sessionDescription': 'This is an example for Android web testing',
  'browserName':        'chrome',
  'deviceGroup':        'KOBITON',
  'deviceName':         'Galaxy*',
  'platformName':       'Android',
  'newCommandTimeout':  120
}

DESIRED_CAPS_ANDROID_APP = {
  'sessionName':        '[Python] Android app',
  'sessionDescription': 'This is an example for Android app testing',
  'app':                'https://s3-ap-southeast-1.amazonaws.com/kobiton-devvn/apps-test/ContactManager.apk',
  'appPackage':         'com.example.android.contactmanager',
  'appActivity':        '.ContactManager',
  'deviceGroup':        'KOBITON',
  'deviceName':         'Galaxy*',
  'platformName':       'Android',
  'newCommandTimeout':  120
}

DESIRED_CAPS_IOS_WEB = {
  'sessionName':        '[Python] iOS web',
  'sessionDescription': 'This is an example for iOS web testing',
  'browserName':        'safari', 
  'deviceGroup':        'KOBITON',
  'deviceName':         'iPhone*',
  'platformName':       'iOS',
  'newCommandTimeout':  120
}

DESIRED_CAPS_IOS_APP_V2 = {
  'sessionName':        'Automation test session',
  'sessionDescription': '',
  'deviceOrientation':  'portrait',
  'captureScreenshots': True,
  # The maximum size of application is 500MB
  # By default, HTTP requests from testing library are expired
  # in 2 minutes while the app copying and installation may
  # take up-to 30 minutes. Therefore, you need to extend the HTTP
  # request timeout duration in your testing library so that
  # it doesn't interrupt while the device is being initialized.
  'app':                'kobiton-store:v348846',
  # The given team is used for finding devices and the created session will be visible for all members within the team.
  'groupId':            2496, # Group: Greet
  'deviceGroup':        'KOBITON',
  # For deviceName, platformVersion Kobiton supports wildcard
  # character *, with 3 formats: *text, text* and *text*
  # If there is no *, Kobiton will match the exact text provided
  'deviceName':         'iPhone*',
  'platformName':       'iOS',
  # 'platformVersion':    '15.0.1'
} 

DESIRED_CAPS_IOS_APP = {
  'sessionName':        '[Python] iOS app',
  'sessionDescription': 'This is an example for iOS app testing',
  'app':                'https://s3-ap-southeast-1.amazonaws.com/kobiton-devvn/apps-test/demo/iFixit.ipa',
  'deviceGroup':        'KOBITON',
  'deviceName':         'iPhone*',
  'platformName':       'iOS',
  'newCommandTimeout':  120
}
