PoC
==============

These are simple samples of how to use Python to run Appium tests. It is suggested that you use a test runner such as pytest or nose.

### Kobiton Credentials
  * Access https://portal.kobiton.com/ with your account
  * Get your username & API Key
  * In the terminal export your Kobiton Credentials as environmental variables:

  ```shell
    export KOBITON_USERNAME="USER"
    export KOBITON_ACCESS_KEY="ACCES_KEY"
    export TEAM="Greet"
    export APP_ID="358344"
  ```


### 1. Pytest

#### Setup

  ```shell
    pip install virtualenv
    virtualenv venv
    source venv/bin/activate
    pip install pytest
    pip install -r requirements.txt
  ```

#### Usage:

##### Run all tests

```shell
  py.test tests
```

##### Run an arbitrary file

```shell
  py.test tests/test_ios_app.py
```


### Resources

- [Selenium Documentation](http://www.seleniumhq.org/docs/)

- [pip](http://pip-installer.org/)

- [Python 2.7 Documentation](https://docs.python.org/2.7/)

- [Python 3.6 Documentation](https://docs.python.org/3.6/)

- [Pytest Documentation](http://pytest.org/latest/contents.html)

- [virtualenv](https://virtualenv.readthedocs.org/en/latest/)

- [Selenium2Library](https://github.com/rtomac/robotframework-selenium2library)

- [Robot Framework](http://code.google.com/p/robotframework/)

- [Appium-client Repository](https://pypi.python.org/simple/appium-python-client)

- [Selenium Repository](https://pypi.python.org/simple/selenium)
